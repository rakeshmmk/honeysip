#! /usr/bin/perl 
### honeysip.pl -- Honeypot SIP server
### Copyright (C) 2012 Rakesh Mukundan { rakesh (at) rakeshmukundan <dot> in }
### This programm is based on PrimiSIP by Emin Gabrielyan 
###
### This is free software; you can redistribute it and/or modify it
### under the terms of the GNU General Public License as published by
### the Free Software Foundation; either version 2, or (at your option)
### any later version.
###
### This program is distributed in the hope that it will be useful, but
### WITHOUT ANY WARRANTY; without even the implied warranty of
### MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
### General Public License for more details.
###
### You should have received a copy of the GNU General Public License
### along with this software; see the file COPYING.  If not, write to
### the Free Software Foundation, 675 Mass Ave, Cambridge, MA 02139,
### USA.


use strict;
use IO::Socket;
use POSIX 'setsid';
use IO::File;
use Getopt::Long;
use Cwd;

###Available extensions to add your own extensions just modify this array
my @extensions = ("100","101","102","admin","service");
#######################################################################
my $honeypot_ip  	 = '';
my $be_daemon   	 = '';
my $allowAllExtn	 = '';
my $consoleDebug	 = '';	

my $version          = "0.1";
my $pwd = cwd(); #get the current path


my $PID_FILE 	= $pwd.'/honeysip.pid'; #PID file for daemon

my $LOG_FILE 	= $pwd.'/daemon.log';  #daemon log file

my $sip_log	= $pwd.'/sip_log.log'; #honeypot log file

my $sip_error	= $pwd.'/sip_error.log';#file to log erraneous sip packets

my $MAX_TO_READ = 2048;
my ($mb, $si, $sp, $method, $rb, $headers, $mline);
my ($ou, $tu, $ft, $fu, $du);
my ($ru, $sendhdr);

my %location=();

######################################################################################
  
my %extension_hash;
for (@extensions) { $extension_hash{$_} = 1 }   #converting the extensions inot a hash with value 1 and key as the extension num for easy processing


######################################################################################



$SIG{TERM} = $SIG{INT} = sub { 

	exit 0;

};


GetOptions('daemon' 		=> \$be_daemon, #run in daemon mode
           'allowAll' 		=> \$allowAllExtn,#allows registering and inviting all extensions
	   'ip=s'		=> \$honeypot_ip,  #External IP of the honeypot system
	   'trace'		=> \$consoleDebug  #Enable Console prints


	);
################## Checking for a valid ip address arguemnt ###############################################################

if($honeypot_ip){

	if($honeypot_ip !~/([0-9]+)\.([0-9]+)\.([0-9]+)\.([0-9]+)/){

		die( "Iinvalid IP \"$honeypot_ip\" Please provide a valid IP address \n");

	}
}
else{

	print "No IP address supplied,using 192.168.1.1 as the SIP server IP addres. This may blow the cover of the honeypot \n";
	$honeypot_ip	= "192.168.1.1";
}


################## Checking for a valid ip address arguemnt ###############################################################
if($consoleDebug){print "------------------Console trace on----------------------------------------------------------- \n"};

if($allowAllExtn){print "------------------Server will consider all extensions as valid------------------------------- \n"};

if( $be_daemon == 1){
	my $fh  = open_pid_file($PID_FILE);
	my $pid = become_daemon();
	print "------------------------------------PID of daemon $pid-----------------------------------------\n";
	print $fh $pid;
	close $fh;
	END { unlink $PID_FILE if $pid == $$; }
}
my $server = IO::Socket::INET->new(
		#LocalAddr=>$me,
		LocalPort=>'5060',
		Proto=>"udp",
		Reuse=>1,
		) or die "Cannot be a udp server: $@\n";

warn "-----------------------Listening on 5060 ----------------------------------------------------------\n";

while (my $user=$server->recv($mb,$MAX_TO_READ))
{
	$si=$server->peerhost;
	$sp=$server->peerport;
	load_parameters();


	#add some code to make sure that its a SIP message, else skip it
	if(validate_sip() ){
		#add the log routine
		log_to_file();	
		   if($method ne "SIP/2.0")
		    {
		      #route();
			  process_sip_msg();
			  if($consoleDebug){
			  	debug_load_values();
			  }
		    }	

	}
	else
	{
		#invalid packet log it in a seperate file
		log_error_packet();	
	}

}
close($server);



#
#Used to send reply msgs to the attacker  uses Local port as 5060 if not specified and given Ip and port as destination 
# send_msg($ip,$desti_port,$msg,[$sources_port] )
#
sub send_msg
{
	my $ip 		= shift;
	my $desti_port	= shift ;
	my $msg		= shift;
	my $source_port 	= shift || '5060';


	my $sock = new IO::Socket::INET (
	#LocalAddr => $me,
	LocalPort => '5060',
	PeerAddr => $ip, 
	PeerPort => $desti_port,
	Proto => 'udp',
	Reuse => 1);
	die "Could not create socket: $!\n" unless $sock;
	print $sock $msg;
	close($sock);
}



sub load_parameters
{
	$mline=$mb;
	$mline=~s/^([^\r\n]*).*$/$1/s;

	$headers=$mb;
	$headers=~s/^[^\r\n]*\r?\n(.*(\r?\n){2}).*$/$1/s;
	$sendhdr=$headers;

	$rb=$mb;
	$rb=~s/^.*(\r?\n){2}(.*)$/$2/s;

	$method=$mline;
	$method=~s/^([^ ]*) .*$/$1/s;

	if($method ne "SIP/2.0")
	{
		$ou=$mline;
		$ou=~s/^[^ ]+ +([^ ]*) .*$/$1/s;

	}
	else
	{
		$ou="";
	}
	$ru=$ou;
	my $rU=$ru;
	$rU=~s/^sip:([^@]*)@.*/$1/;

	$du="";

	$tu=$headers;
	$tu=~s/.*(^|\n)To:[^\n<]*<([^>]*)>.*$/$2/s;

	$fu=$headers;
	$fu=~s/.*(^|\n)From:[^\n<]*<([^>]*)>.*$/$2/s;

	$ft=$headers;
	$ft=~s/.*(^|\n)From:[^\n<]*<[^>]*>[\n]*;tag=([^;\r\n]*).*/$2/s;
}

sub hdr_query
{
	my $field = shift;
	my $s=$headers;
	$s=~s/(^|\n)(?!$field)[^\n]*/$1/gs;
	$s=~s/^\n*//gs;
	$s=~s/\n+/\n/gs;
	return $s;
}

sub sl_send_reply
{
	my $code = shift;
	my $text = shift;
	my $msg=
	"SIP/2.0 $code $text\r\n" .
	hdr_query("Via|From|To|CSeq|Call-ID") .
	"Server: PrimiSIP\r\n" .
	"Content-Length: 0\r\n" .
	"\r\n";
	send_msg($si,$sp,$msg);
}

sub sl_reply_error
{
	sl_send_reply("500","Server error occured");
}


sub reply_invite
{
	my $code = shift;
	my $text = shift;
	my $msg=
	"SIP/2.0 $code $text\r\n" .
	hdr_query("Via|From|To|CSeq|Call-ID") .
	#"Server: PrimiSIP\r\n" .
	"User-Agent: Asterisk PBX 1.6.0.10-FONCORE-r40\r\n" .
	"Allow: INVITE, ACK, CANCEL, OPTIONS, BYE, REFER, SUBSCRIBE, NOTIFY\r\n" .
	"Supported: replaces, timer\r\n" .
	"Contact: <sip:100@".$honeypot_ip." >\r\n" .
	"Content-Length: 0\r\n" .
	"\r\n";
	send_msg($si,$sp,$msg);
}
sub sl_send_reply_options
{
	my $code = shift;
	my $text = shift;
	my $msg=
	"SIP/2.0 $code $text\r\n" .
	hdr_query("Via|From|To|Call-ID|CSeq") .
	#"Server: PrimiSIP\r\n" .
	"User-Agent: Asterisk PBX 1.6.0.10-FONCORE-r40\r\n" .
	"Allow: INVITE, ACK, CANCEL, OPTIONS, BYE, REFER, SUBSCRIBE, NOTIFY\r\n" .
	"Supported: replaces, timer\r\n" .
	"Contact: <sip:100@".$honeypot_ip." r\n" .
	"Content-Length: 0\r\n" .
	"\r\n";
	send_msg($si,$sp,$msg);
}

my $sdp_data_asterisk = "v=0\r\n" .
"o=root 367422576 367422576 IN IP4".$honeypot_ip." \r\n" .
"s=Asterisk PBX 1.6.0.10-FONCORE-r40\r\n" .
"c=IN IP4 ".$honeypot_ip."\r\n" .
"b=CT:384\r\n" .
"t=0 0\r\n" .
"m=audio 17558 RTP/AVP 0 8\r\n" .
"a=rtpmap:0 PCMU/8000\r\n" .
"a=rtpmap:8 PCMA/8000\r\n" .
"a=silenceSupp:off - - - -\r\n" .
"a=ptime:20\r\n" .
"a=sendrecv\r\n" .
"m=video 17850 RTP/AVP 34 99\r\n" .
"a=rtpmap:34 H263/90000\r\n" .
"a=rtpmap:99 H264/90000\r\n" .
"a=sendrecv\r\n";



sub validate_sip
{

	if($method eq "SIP/2.0")
	{
		return 1;
	}
	elsif($method=~m/(^INVITE$|^CANCEL$|^ACK$|^REGISTER|^OPTIONS|^BYE)/)
	{
		return 1;
	}
	else
	{
		return 0;
	}
}

sub log_to_file
{
	my $log_file;
	my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst)=localtime(time); 
	open ($log_file, ">>$sip_log") or die $!;

	print $log_file "Source : $si:$sp \n";
	print $log_file "Time : "; # add time stamp
	printf $log_file "%4d-%02d-%02d %02d:%02d:%02d\n\n",$year+1900,$mon+1,$mday,$hour,$min,$sec;

	print $log_file "Message \n\n";
	print $log_file $mb;
	print $log_file "\n\n\n---------------------------------------------------\n";

	close($log_file);
}
sub log_error_packet
{
	my $log_file;
	my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst)=localtime(time); 
	open $log_file, ">>$sip_error" or die $!;

	print $log_file "Source : $si:$sp \n";
	print $log_file "Time : "; # add time stamp
	printf $log_file "%4d-%02d-%02d %02d:%02d:%02d\n\n",$year+1900,$mon+1,$mday,$hour,$min,$sec;

	print $log_file "Message \n\n";
	print $log_file $mb;
	print $log_file "\n\n\n---------------------------------------------------\n";

	close($log_file);

}

sub process_sip_msg
{
	if($method eq "INVITE")
	{
		sl_send_reply("100","Giving a Try");

		$ou =~/sip:(.+)@/;  #extract the extension number
		if(search_extensions ($1)){

			#print "Found Extension \n";
			sl_send_reply("200","OK");

	}
	else
	{
		#print "Extension not found \n";
		sl_send_reply("404","Not Found");
	}



	#record_route();
	#process_invite();
	}

	if($method eq "OPTIONS")
	{
		sl_send_reply_options("200","OK");
	}
	if($method eq "BYE")
	{
		sl_send_reply("200","OK");
	}

	if($method eq "REGISTER")
	{
		
		$ou =~/sip:(.+)@/;  #extract the extension number
		if(search_extensions ($1)){

			#print "Found Extension \n";
			sl_send_reply("200","OK");

		}
		else
		{
			#print "Extension not found \n";
			sl_send_reply("404","Not Found");
		}

  
 	 }




}

sub search_extensions
{
	my $extn = shift;
	if ($allowAllExtn){
	
	    return 1;
	}
	else{
	    if ($extension_hash{$extn}){
			    return 1;
	    }
	    else
	    {
			    return 0;
	    }

    }

}

sub debug_load_values
{

	print "mb =$mb \n si=$si\n sp=$sp\n  method=$method\n rb=$rb\n headers=$headers\n mline=$mline \n";
	print "ou=$ou \n tu=$tu\n ft=$ft \n  fu=$fu\n du=$du\n";
	print "ru=$ru\n sendhdr=$sendhdr \n";



}

sub become_daemon{
	die "Can't fork $! " unless defined (my $child	= fork );
	exit 0 if $child; #parent dies
	setsid();	#become session leader
	open(STDIN,"</dev/null");
	open(STDOUT,">$LOG_FILE");
	open(STDERR,">&STDOUT");
	chdir '/';	#change working directory
	umask(0);	#forget file mode creation mask
	$ENV{PATH}	='/bin:/usr/bin:/usr/sbin';
	return $$;

}

sub open_pid_file{
	my $file	= shift;
	if(-e $file ){		#PID already exists
	my $fh		= IO::File->new($file)	|| return;
	my $pid		= <$fh>;
	die "Server already running with PID $pid " if kill 0 => $pid;
	warn 'Removing PID file for defunct server process $pid. \n';
        die " Can't unlink PID file $file " unless -w $file && unlink $file;
	}
	return IO::File->new($file,O_WRONLY|O_CREAT|O_EXCL,0644)
	or die "Can't create $file $! \n";
	
}








